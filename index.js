const { promisify } = require("es6-promisify");
const generate_page = require("./components/page.js");
const xmlserializer = require("xmlserializer");
const fs = require("fs-extra");
const svg2imgc = require("svg2img");
const svg2img = promisify(svg2imgc);
const path = require("path");

const args = require("./args.js");

const main = async ({ output, num, no_xml, clean }) => {
  const initial = Date.now();
  let before = Date.now();
  for (var i = 0; i < num; i++) {
    const comp = generate_page();

    svgString = xmlserializer.serializeToString(comp.svg);
    const buffer = await svg2img(svgString, comp.img);

    let tasks = [
      fs.writeFile(path.join(output, `${comp.filename}.jpg`), buffer)
    ];
    if (!no_xml) {
      tasks.push(
        fs.writeFile(path.join(output, `${comp.filename}.xml`), comp.xmlString)
      );
    }
    await Promise.all(tasks);

    if (i % 1000 == 1000 - 1 && i != 0) {
      console.log("i:", i + 1);
      console.log("interval:", (Date.now() - before) / 1000, "seconds");
      console.log("elapsed:", (Date.now() - initial) / 1000 / 60, "minutes");
      console.log();
      before = Date.now();
    }
  }
  const after = Date.now();
  console.log("total time:", (after - initial) / 1000 / 60, "minutes");
};

main(args);
