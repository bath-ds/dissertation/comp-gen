const fs = require('fs-extra');
const random = require('random');
const initSVG = require('../utils/init-svg.js');
const consts = require('../utils/consts.js')
const { randomComponent, randomPage, randomNav } = require('../utils/random-component.js');
const randomHue = require('../utils/random-hue.js');
const generate_id = require('../utils/generate-id.js');

const annotationTemplate = fs.readFileSync('annotation.xml', 'utf8');
const objectTemplate = fs.readFileSync('object.xml', 'utf8');

const generate_page = () => {
  const { svg, rc } = initSVG();

  // define top left position, width, and other basic properties
  const p_width = random.float(150, 250);
  const p_x = random.float(consts.canvas.padding * 4, consts.canvas.width - consts.canvas.padding - 20);
  const p_y = random.float(consts.canvas.padding * 4, consts.canvas.height - consts.canvas.padding - 20);
  const p_x_mid = (p_x * 2 + p_width) / 2;
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: 'black',
    strokeWidth: random.float(.5, 1.5),
  };
  const pagePadding = random.float(10, 20);
  const rows = random.int(4, 10); // rows of components
  const xmlObjects = [];
  const right_xs = [p_x + p_width];
  const bottom_ys = [];
  let prev_comp;

  // rows loop
  let row_y = p_y;
  for (let i = 0; i < rows; i++) {
    const isTitleBar = random.boolean();
    if (i == 0 && isTitleBar) {
      const generate = require('./title-bar.js');
      const comp = generate(svg, rc, {
        row: i,
        x: p_x,
        y: row_y,
        x_mid: (p_x * 2 + p_width) / 2,
        width: p_width,
        padding: pagePadding,
        options,
      });
      row_y = comp.ymax - random.float(0, 5);
      prev_comp = comp;

      // compose xml
      const xmlString = objectTemplate.replace('{type}', comp.type)
        .replace('{xmin}', comp.bndbox.xmin)
        .replace('{ymin}', comp.bndbox.ymin)
        .replace('{xmax}', comp.bndbox.xmax)
        .replace('{ymax}', comp.bndbox.ymax);
      xmlObjects.push(xmlString);
    } else {
      const cols = random.int(1, 2);
      const col_width = (cols == 1) ? p_width : p_width / 2;
      const max_ys = [];
      for (let j = 0; j < cols; j++) {
        const generate = randomComponent(i);

        // draw
        const col_x = (j == 0) ? p_x : p_x_mid;
        const comp = generate(svg, rc, {
          row: i,
          x: col_x,
          y: row_y,
          x_mid: (col_x * 2 + col_width) / 2,
          width: col_width,
          padding: pagePadding,
          multicols: cols > 1,
          options,
          prev_comp,
        });
        max_ys.push(comp.ymax);
        bottom_ys.push(comp.ymax);
        prev_comp = comp;

        // compose xml
        const xmlString = objectTemplate.replace('{type}', comp.type)
          .replace('{xmin}', comp.bndbox.xmin)
          .replace('{ymin}', comp.bndbox.ymin)
          .replace('{xmax}', comp.bndbox.xmax)
          .replace('{ymax}', comp.bndbox.ymax);
        xmlObjects.push(xmlString);
      }
      row_y = Math.max(...max_ys) - random.float(0, 5);
    }
  }

  // draw page borders
  const p_height = Math.max(...bottom_ys) - p_y + pagePadding;
  let node = rc.rectangle(p_x, p_y, p_width, p_height, options); // x, y, width, height
  svg.appendChild(node);

  // draw page identifier or not
  const drawId = random.float(0, 1) < .95;
  if (drawId) {
    const generate = randomPage();
    const comp = generate(svg, rc, {
      x: p_x,
      y: p_y,
      x_mid: (p_x * 2 + p_width) / 2,
      width: p_width,
      padding: pagePadding,
      options,
    });

    // compose xml
    const xmlString = objectTemplate.replace('{type}', comp.type)
      .replace('{xmin}', comp.bndbox.xmin)
      .replace('{ymin}', comp.bndbox.ymin)
      .replace('{xmax}', comp.bndbox.xmax)
      .replace('{ymax}', comp.bndbox.ymax);
    xmlObjects.push(xmlString);
  }

  // draw navigations or not
  let navPos = ['top', 'right', 'bottom', 'left'];
  if (drawId) {
    navPos.shift();
  }
  for (let i = 0; i < navPos.length; i++) {
    const drawNav = random.float(0, 1) < .95;
    if (drawNav) {
      const generate = randomNav();
      const comp = generate(svg, rc, {
        x: p_x,
        y: p_y,
        x_mid: (p_x * 2 + p_width) / 2,
        width: p_width,
        height: p_height,
        padding: pagePadding,
        options,
        pos: navPos[i],
      });
      right_xs.push(comp.xmax);
      bottom_ys.push(comp.ymax);

      // compose xml
      const xmlString = objectTemplate.replace('{type}', comp.type)
        .replace('{xmin}', comp.bndbox.xmin)
        .replace('{ymin}', comp.bndbox.ymin)
        .replace('{xmax}', comp.bndbox.xmax)
        .replace('{ymax}', comp.bndbox.ymax);
      xmlObjects.push(xmlString);
    }
  }

  // finalize xml
  filename = generate_id();
  let xmlString = annotationTemplate.replace(/{filename}/g, filename)
    .replace('{width}', 500)
    .replace('{height}', 667);
  xmlString += '\n';
  xmlString += xmlObjects.join('\n');
  xmlString += '\n</annotation>';

  // return final dimensions
  return {
    svg,
    xmlString,
    filename,
    img: {
      format: consts.canvas.format,
      quality: consts.canvas.quality,
      width: Math.max(consts.canvas.minWidth, Math.max(...right_xs) + consts.canvas.padding),
      height: Math.max(consts.canvas.minHeight, Math.max(...bottom_ys) + consts.canvas.padding),
    },
  };
};

module.exports = generate_page;