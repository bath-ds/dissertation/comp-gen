const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_radio_button = (svg, rc, props) => {
  // START generate left circle
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, props.x_mid - props.x + props.padding));
  const r_y = props.y + props.padding;
  const r_width = random.float(8, 13);
  const r_height = random.float(8, 13);
  const options = {
    roughness: random.float(.5, .8),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  const diameter = random.float(7, 12)
  const circle_y = (r_y * 2 + r_height) / 2 + random.float(0, 2);
  let node = rc.circle(r_x + diameter, circle_y, diameter, options); // x, y, width, height
  svg.appendChild(node);
  // END generate left circle

  // START generate right line
  options.roughness = random.float(.5, .8);
  const l_width = random.float(40, 80);
  const x_max = Math.min(props.x + props.width - props.padding, r_x + r_width + l_width);
  node = rc.line(
    r_x + diameter + random.float(8, 14), 
    (r_y * 2 + r_height) / 2 + random.float(0, 2), 
    x_max, 
    (r_y * 2 + r_height) / 2 + random.float(0, 2), 
    options
  ); // x1, y1, x2, y2
  svg.appendChild(node);
  // END generate right line

  return responseBuilder(
    'radio-button',
     r_x + 2,
     r_y,
     x_max,
     r_height + r_y + 2
  );
};

module.exports = generate_radio_button;
