const random = require('random');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');
const emnist = require('../utils/emnist-generator.js');

const generate_nav_a = (svg, rc, props) => {
  let r_x, r_y, r_width, r_height;
  if (props.pos == 'top') {
    r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, 30));
    r_y = props.y - random.float(30, 50);
    r_width = Math.min(props.width - props.padding, (props.x_mid - r_x) * 2);
    r_height = props.y - r_y - random.float(0, 4);
  } else if (props.pos == 'right') {
    r_x = props.x + props.width + random.float(0, 4);
    r_y = props.y + random.float(30, props.height-20);
    r_width = random.float(20,40);
    r_height = random.float(20, 40);
  } else if (props.pos == 'left') {
    r_width = random.float(20,40);
    r_x = props.x - r_width - random.float(0, 4);
    r_y = props.y + random.float(30, props.height-20);
    r_height = random.float(20, 40);
  } else {
    r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, 30));
    r_y = props.y + props.height + random.float(0, 4);
    r_width = random.float(20,40);
    r_height = random.float(20, 40);
  }

  // START generate diamond
  x_mid = (r_x * 2 + r_width) / 2;
  y_mid = (r_y * 2 + r_height) / 2;

  d_top = [x_mid + random.float(-1, 1), r_y + random.float(-1, 1)];
  d_left = [r_x + random.float(-1, 1), y_mid + random.float(-1, 1)];
  d_bottom = [x_mid + random.float(-1, 1), r_y + r_height + random.float(-1, 1)];
  d_right = [r_x + r_width + random.float(-1, 1), y_mid + random.float(-1, 1)];

  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  let node = rc.polygon([d_top, d_left, d_bottom, d_right], options); // x, y, width, height
  svg.appendChild(node);
  // END generate diamond

  const letter = emnist('a', {'x': x_mid, 'y': y_mid});
  svg.appendChild(letter);

  return responseBuilder(
    'nav-a',
     r_x,
     r_y,
     r_width + r_x,
     r_height + r_y
  );
};

module.exports = generate_nav_a;

