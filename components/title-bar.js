const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_title_bar = (svg, rc, props) => {
  // START generate outer rectangle bottom line
  const r_height = random.float(props.width * .15, props.width * .2);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: props.options.stroke,
    strokeWidth: random.float(.5, 1.5),
  };

  let node = rc.line(
    props.x,
    props.y + r_height,
    props.x + props.width,
    props.y + r_height,
    options
  );
  svg.appendChild(node);
  // END generate outer rectangle bottom line

  // START generate chevron
  options.roughness = random.float(.5, .8);
  x_mid = (props.x * 2 + props.width) / 2;
  y_mid = (props.y * 2 + r_height) / 2;

  p1 = [props.x + random.float(18, 22), props.y + random.float(2, 8)];
  p2 = [props.x + random.float(6, 12), y_mid + random.float(-1, 1)];
  p3 = [props.x + random.float(18, 22), props.y + r_height - random.float(2, 8)];

  node = rc.linearPath([p1, p2, p3], options);
  svg.appendChild(node);
  // END generate chevron

  // START generate heading
  options.roughness = random.float(.5, .8);
  options.fill = props.options.stroke;
  options.fillStyle = 'solid';
  options.strokeWidth = random.float(1, 2);

  node = rc.rectangle(
    x_mid - random.float(20, 30),
    y_mid - random.float(5, 7),
    random.float(20, 35) * 2,
    random.float(3, 4) * 2,
    options
  ); // x, y, width, height
  svg.appendChild(node);
  // END generate heading

  return responseBuilder(
    'title-bar',
     props.x,
     props.y,
     props.x + props.width,
     props.y + r_height
  );
};

module.exports = generate_title_bar;
