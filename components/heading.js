const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_heading = (svg, rc, props) => {
  // START generate outer rectangle
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, 50));
  const r_y = props.y + props.padding;
  const r_width = Math.min(props.width - props.padding, (props.x_mid - r_x) * 2);
  const r_height = random.float(r_width * .1, r_width * .13);
  hue = randomHue();
  const options = {
    roughness: random.float(.5, 1),
    bowing: 0,
    stroke: hue,
    strokeWidth: random.float(1, 2),
    fill: hue,
    fillStyle: 'solid',
  };

  let node = rc.rectangle(
    r_x,
    r_y,
    r_width,
    r_height,
    options
  ); // x, y, width, height
  svg.appendChild(node);
  // END generate outer rectangle

  return responseBuilder(
    'heading',
     r_x,
     r_y,
     r_width + r_x + 1,
     r_height + r_y + 1
  );
};

module.exports = generate_heading;

