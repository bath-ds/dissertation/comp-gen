const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_checkbox = (svg, rc, props) => {
  // START generate left square
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, props.x_mid - props.x + props.padding));
  const r_y = props.y + props.padding;
  const r_width = random.float(8, 13);
  const r_height = random.float(8, 13);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  let node = rc.rectangle(
    r_x,
    r_y,
    r_width,
    r_height,
    options
  ); // x, y, width, height
  svg.appendChild(node);
  // END generate left square

  // START generate right line
  options.roughness = random.float(.5, .8);
  const l_width = random.float(40, 80);
  const x_max = Math.min(props.x + props.width - props.padding, r_x + r_width + l_width);
  node = rc.line(
    r_x + r_width + random.float(5, 12), 
    (r_y * 2 + r_height) / 2 + random.float(0, 2), 
    x_max, 
    (r_y * 2 + r_height) / 2 + random.float(0, 2), 
    options
  ); // x1, y1, x2, y2
  svg.appendChild(node);
  // END generate right line

  return responseBuilder(
    'checkbox',
     r_x,
     r_y,
     x_max,
     r_height + r_y
  );
};

module.exports = generate_checkbox;
