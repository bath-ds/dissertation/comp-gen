const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_text = (svg, rc, props) => {
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, props.x_mid - props.x + props.padding));
  const r_y = props.y + props.padding;
  const r_width = random.float(40, 80);
  const r_height = random.float(10, 13);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  // START generate left line
  let node = rc.line(
    r_x,
    r_y,
    r_x, 
    r_y + r_height,
    options
  ); // x1, y1, x2, y2
  svg.appendChild(node);
  // END generate left line

  // START generate horizontal line
  options.roughness = random.float(.5, .8);
  const x_max = Math.min(props.x + props.width - props.padding, r_x + r_width);
  // const x_max = r_x + r_width + random.float(40, 80)
  node = rc.line(
    r_x + random.float(0, 1), 
    (r_y * 2 + r_height) / 2 + random.float(0, 1), 
    x_max - random.float(0, 1), 
    (r_y * 2 + r_height) / 2 + random.float(0, 1), 
    options
  ); // x1, y1, x2, y2
  svg.appendChild(node);
  // END generate horizontal line

  // START generate right line
  options.roughness = random.float(.5, 1.5);
  node = rc.line(
    x_max,
    r_y,
    x_max, 
    r_y + r_height,
    options
  ); // x1, y1, x2, y2
  
  svg.appendChild(node);
  // END generate right line

  return responseBuilder(
    'text',
     r_x,
     r_y,
     x_max,
     r_height + r_y
  );
};

module.exports = generate_text;

