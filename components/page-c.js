const random = require('random');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');
const emnist = require('../utils/emnist-generator.js');

const generate_page_c = (svg, rc, props) => {
  // START generate outer rectangle
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, 30));
  const r_width = Math.min(props.width - props.padding, (props.x_mid - r_x) * 2);
  const r_height = random.float(r_width * .5, r_width);
  const r_y = props.y - r_height - random.float(3, 10);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  let node = rc.rectangle(
    r_x,
    r_y,
    r_width,
    r_height,
    options
  ); // x, y, width, height
  svg.appendChild(node);
  // END generate outer rectangle

  const x_mid = (r_x * 2 + r_width) / 2;
  const y_mid = (r_y * 2 + r_height) / 2;
  const letter = emnist('C', {'x': x_mid, 'y': y_mid});
  svg.appendChild(letter);

  return responseBuilder(
    'page-c',
     r_x,
     r_y,
     r_width + r_x,
     r_height + r_y
  );
};

module.exports = generate_page_c;


