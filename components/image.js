const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_image = (svg, rc, props) => {
  const r_height = random.float(props.width * .3, props.width * .5);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };
  let xmin = 0;
  let ymin = 0;
  let xmax = 0;
  let ymax = 0;

  // depends on full width or not, also depends on row
  const fullWidth = random.boolean() && !props.multicols;
  if (fullWidth) {
    xmin = props.x;
    xmax = props.x + props.width;
    ymax = props.y + props.padding + r_height;
    // check first row, if first row only draw bottom line
    // draw bottom line
    let node = rc.line(
      xmin,
      ymax,
      xmax,
      ymax,
      options
    );
    svg.appendChild(node);
    
    // draw top
    ymin = props.y + props.padding;
    const prevCond = props.row > 0 && ((props.prev_comp.type == 'image' && props.prev_comp.fullWidth) || (props.prev_comp.type == 'title-bar' && !props.multicols));
    if (prevCond) {
      ymin = props.prev_comp.ymax + random.float(-1, 1);
    }
    if (props.row > 0 && !prevCond) {
      node = rc.line(
        xmin,
        ymin,
        xmax,
        ymin,
        options
      );
      svg.appendChild(node);
    }
  } else {
    xmin = Math.max(props.x + props.padding, props.x_mid - random.float(20, 50));
    ymin = props.y + props.padding;
    const r_width = Math.min(props.width - props.padding, (props.x_mid - xmin) * 2);
    const r_height = random.float(r_width * .3, r_width * .5);
    xmax = xmin + r_width;
    ymax = ymin + r_height;

    node = rc.rectangle(xmin, ymin, r_width, r_height, options); // x, y, width, height
    svg.appendChild(node);
  }

  // START generate top left diagonal line
  // options.roughness = random.float(.5, .8);
  node = rc.line(
    xmin + random.float(-1, 1),
    ymin + random.float(-1, 1),
    xmax + random.float(-1, 1), 
    ymax + random.float(-1, 1), 
    options
  ); // x1, y1, x2, y2
  svg.appendChild(node);
  // END generate top left diagonal line

  // START generate bottom left diagonal line
  node = rc.line(
    xmin + random.float(-1, 1),
    ymax + random.float(-1, 1), 
    xmax + random.float(-1, 1), 
    ymin + random.float(-1, 1),
    options
  ); // x1, y1, x2, y2
  svg.appendChild(node);
  // END generate bottom left diagonal line

  return responseBuilder(
    'image',
     xmin,
     ymin,
     xmax,
     ymax,
     fullWidth
  );
};

module.exports = generate_image;
