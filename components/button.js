const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_button = (svg, rc, props) => {
  // START generate outer rectangle
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, 50));
  const r_y = props.y + props.padding;
  const r_width = Math.min(props.width - props.padding, (props.x_mid - r_x) * 2);
  const r_height = random.float(r_width * .3, r_width * .4);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  let node = rc.rectangle(
    r_x,
    r_y,
    r_width,
    r_height,
    options
  ); // x, y, width, height
  svg.appendChild(node);
  // END generate outer rectangle

  // START generate squiggly line
  options.roughness = random.float(.5, .8);
  const points = [];

  const s_x = r_x + random.float(5, 10);
  const s_y = r_y;

  function norm(val, rmin, rmax, tmin, tmax) { return ((val - rmin) / (rmax - rmin)) * (tmax - tmin) + tmin; }

  const s_width = Math.max(r_width - random.float(20, 30), 35);
  const s_min = norm(r_height, 35*.3, 50*.8, 2, 6);
  const s_max = norm(r_height, 35*.3, 50*.8, 15, 21);
  const s_period = random.float(10, 15);
  const s_offset = random.float(0, 1000);
  for (let xi = s_x; xi < s_x + s_width; xi++) {
    const yi = g.triangleWave(xi, s_min, s_max, s_period, s_offset);
    points.push([xi, s_y + yi]);
  }

  node = rc.linearPath(points, options);
  svg.appendChild(node);
  // END generate squiggly line

  return responseBuilder(
    'button',
     r_x,
     r_y,
     r_width + r_x,
     r_height + r_y
  );
};

module.exports = generate_button;