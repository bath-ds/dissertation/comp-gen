const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_textbox = (svg, rc, props) => {
  // START generate outer rectangle
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(30, 60));
  const r_y = props.y + props.padding;
  const r_width = Math.min(props.width - props.padding, (props.x_mid - r_x) * 2);
  const r_height = random.float(r_width * .25, r_width * .3);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  let node = rc.rectangle(
    r_x,
    r_y,
    r_width,
    r_height,
    options
  ); // x, y, width, height
  svg.appendChild(node);
  // END generate outer rectangle

  // START generate left line
  options.roughness = random.float(.5, .8);
  node = rc.line(
    r_x + random.float(5, 9), 
    r_y + random.float(2, 4), 
    r_x + random.float(5, 9), 
    r_y + r_height - random.float(2, 4), 
    options
  ); // x1, y1, x2, y2
  svg.appendChild(node);
  // END generate left line

  return responseBuilder(
    'textbox',
     r_x,
     r_y,
     r_width + r_x,
     r_height + r_y
  );
};

module.exports = generate_textbox;
