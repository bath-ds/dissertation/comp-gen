const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_dropdown = (svg, rc, props) => {
  // START generate outer rectangle
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, 50));
  const r_y = props.y + props.padding;
  const r_width = Math.min(props.width - props.padding, (props.x_mid - r_x) * 2);
  const r_height = random.float(r_width * .3, r_width * .35);
  const options = {
    roughness: random.float(.5, 1.5),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };

  let node = rc.rectangle(
    r_x,
    r_y,
    r_width,
    r_height,
    options
  ); // x, y, width, height
  svg.appendChild(node);
  // END generate outer rectangle

  // START generate chevron
  options.roughness = random.float(.5, .8);

  p1 = [r_x + r_width - random.float(16, 20), r_y + random.float(2, 5)];
  p2 = [r_x + r_width - random.float(10, 14), r_y + r_height - random.float(2, 5)];
  p3 = [r_x + r_width - random.float(4, 8), r_y + random.float(2, 5)];

  node = rc.linearPath([p1, p2, p3], options);
  svg.appendChild(node);
  // END generate chevron

  return responseBuilder(
    'dropdown',
     r_x,
     r_y,
     r_width + r_x,
     r_height + r_y
  );
};

module.exports = generate_dropdown;