const random = require('random');
const g = require('g.js');
const responseBuilder = require('../utils/response-builder.js');
const randomHue = require('../utils/random-hue.js');

const generate_paragraph = (svg, rc, props) => {
  // START generate paragraph
  const r_x = Math.max(props.x + props.padding, props.x_mid - random.float(20, props.x_mid - props.x + props.padding));
  const r_y = props.y + props.padding;
  const r_width = random.float(40, 80);
  const x_max = Math.min(props.x + props.width - props.padding, r_x + r_width + r_width);
  const options = {
    roughness: random.float(.5, .8),
    bowing: 0,
    stroke: randomHue(),
    strokeWidth: random.float(.5, 1.5),
  };
  const spacing = random.float(5, 7);

  // first line
  let node = rc.line(
    r_x,
    r_y,
    x_max + random.float(-1, 1),
    r_y,
    options
  );
  svg.appendChild(node);

  // second line
  node = rc.line(
    r_x,
    r_y + spacing + random.float(-1, 1),
    x_max + random.float(-1, 1),
    r_y + spacing + random.float(-1, 1),
    options
  );
  svg.appendChild(node);

  // third line
  node = rc.line(
    r_x,
    r_y + spacing * 2 + random.float(-1, 1),
    x_max + random.float(-1, 1),
    r_y + spacing * 2 + random.float(-1, 1),
    options
  );
  svg.appendChild(node);

  // fourth line
  node = rc.line(
    r_x,
    r_y + spacing * 3 + random.float(-1, 1),
    x_max + random.float(-1, 1),
    r_y + spacing * 3 + random.float(-1, 1),
    options
  );
  svg.appendChild(node);

  const r_height = (r_y + spacing * 3) - r_y + 1;
  // END generate paragraph

  return responseBuilder(
    'paragraph',
     r_x,
     r_y,
     x_max,
     r_height + r_y
  );
};

module.exports = generate_paragraph;
