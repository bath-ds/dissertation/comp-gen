const button = require('../components/button.js');
const checkbox = require('../components/checkbox.js');
const dropdown = require('../components/dropdown.js');
const heading = require('../components/heading.js');
const paragraph = require('../components/paragraph.js');
const radioButton = require('../components/radio-button.js');
const text = require('../components/text.js');
const textbox = require('../components/textbox.js');
const image = require('../components/image.js');

const components = [
  button,
  checkbox,
  dropdown,
  heading,
  paragraph,
  radioButton,
  text,
  textbox,
  image,
];

const page_a = require('../components/page-a.js');
const page_b = require('../components/page-b.js');
const page_c = require('../components/page-c.js');
const page_d = require('../components/page-d.js');

const pages = [
  page_a,
  page_b,
  page_c,
  page_d,
];

const nav_a = require('../components/nav-a.js');
const nav_b = require('../components/nav-b.js');
const nav_c = require('../components/nav-c.js');
const nav_d = require('../components/nav-d.js');

const navs = [
  nav_a,
  nav_b,
  nav_c,
  nav_d,
];

// might return function
module.exports = {
  randomComponent: row => {
    return components[Math.floor(Math.random() * components.length)];
  },
  randomPage: () => {
    return pages[Math.floor(Math.random() * pages.length)];
  },
  randomNav: () => {
    return navs[Math.floor(Math.random() * navs.length)];
  },
};