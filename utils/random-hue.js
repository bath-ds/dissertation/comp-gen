const hues = ['red', 'green', 'blue', 'black']

module.exports = () => {
  return hues[Math.floor(Math.random()*hues.length)];
};