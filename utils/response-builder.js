const consts = require('./consts.js')

module.exports = (type, xmin, ymin, xmax, ymax) => ({
  type,
  xmin: parseInt(xmin),
  ymin: parseInt(ymin),
  xmax: parseInt(xmax),
  ymax: parseInt(ymax),
  bndbox: {
    xmin: parseInt(xmin - consts.bndbox.padding),
    ymin: parseInt(ymin - consts.bndbox.padding),
    xmax: parseInt(xmax + consts.bndbox.padding),
    ymax: parseInt(ymax + consts.bndbox.padding),
  },
});