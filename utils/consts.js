module.exports = {
  canvas: {
    padding: 20,
    width: 500,
    height: 667,
    format: 'jpg',
    quality: 50,
    minWidth: 500,
    minHeight: 667,
  },
  bndbox: {
    padding: 3,
  },
};