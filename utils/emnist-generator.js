const random = require("random");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const DOMParser = new JSDOM().window.DOMParser;

const charMap = {
  A: 10,
  B: 11,
  C: 12,
  D: 13,
  a: 36,
  b: 37,
  c: 12,
  d: 38
};

module.exports = (letter, mid) => {
  const lower = ['a', 'b', 'c', 'd'].some(i => i == letter);
  const dim = random.float(lower ? 16 : 20, lower ? 19 : 29);
  const fileNum = random.int(0, 2399);

  const x = mid.x - dim / 2 + random.float(-2, 2);
  const y = mid.y - dim / 2 + random.float(-2, 2);

  // pick image based on letter
  var xmlString = `<image x="${x}" y="${y}" width="${dim}" height="${dim}" href="emnist/${
    charMap[letter]
  }/${fileNum}.png" />`;
  doc = new DOMParser().parseFromString(xmlString, "text/xml");
  node = doc.firstChild;

  return node;
};