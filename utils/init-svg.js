const random = require('random');
const rgbHex = require('rgb-hex');
const rough = require('roughjs');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

module.exports = (apply_background = true) => {
  const { document } = (new JSDOM(`<!DOCTYPE html><svg id="svg"></svg>`)).window;
  svg = document.getElementById('svg');
  const rc = rough.svg(svg);

  // apply white background
  if (apply_background) {
    const col = random.int(200, 255);
    let bg = rc.rectangle(-2, -2, 5000, 5000, { roughness: 0, bowing: 0, strokeWidth: 0, fill: '#' + rgbHex(col, col, col), fillStyle: 'solid' }); // x, y, width, height
    svg.appendChild(bg);
  }

  return { svg, rc };
};