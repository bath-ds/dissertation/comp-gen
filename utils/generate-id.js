const generate_id = require('nanoid/generate');

const alphabet = '0123456789abcdefghijklmnopqrstuvwxyz';

module.exports = () => generate_id(alphabet, 20);