# comp-gen

Generates low-fidelity user interface sketch to be used as training data for a [Tensorflow Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection) model. This project heavily depends on both the [Rough.js](https://roughjs.com/) and [EMNIST](https://www.nist.gov/node/1298471/emnist-dataset) projects.

See the main [Lines of Code repository](https://gitlab.com/bath-ds/dissertation/lines-of-code).

## Prerequisites

- [Node.js](https://nodejs.org/en/)

## Installation

Clone the repository and install all required packages by executing `npm install`.

## Running

Run the `index.js` script with the following parameters.

    node index.js <output_directory> <num_images>

    output_directory:
      Full path to the output directory where the image files will be generated.

    num_images:
      The number of training images to generate.

## Example Output Image

![Example output image](https://gitlab.com/bath-ds/dissertation/comp-gen/raw/master/example.jpg)
