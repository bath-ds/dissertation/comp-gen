const fs = require("fs-extra");
const ArgumentParser = require("argparse").ArgumentParser;
const parser = new ArgumentParser();

parser.addArgument("output", {
  help: "Full path to the output directory.",
  type: arg => {
    try {
      if (!fs.lstatSync(arg).isDirectory()) {
        parser.error(`Output is not a directory: ${arg}`);
      }
    } catch (err) {
      parser.error(`Output directory does not exist: ${arg}`);
    }
    return arg;
  }
});
parser.addArgument("num", {
  help: "Number of training images to generate.",
  type: "int"
});
parser.addArgument(["-n", "--no-xml"], {
  help: "Disable xml output.",
  action: "storeTrue"
});
parser.addArgument(["-c", "--clean"], {
  help: "If specified, clean (remove files in) the output directory first.",
  action: "storeTrue"
});

module.exports = parser.parseArgs();